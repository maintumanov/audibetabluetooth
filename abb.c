#include <abb.h>

unsigned int16 timer = 0;
unsigned int16 rtimer = 0;
unsigned int16 offtimer = 0;
int1 relay_state = 0;

void main()
{

  setup_timer_2(T2_DIV_BY_16, 0, 1); //1,0 us overflow, 1,0 us interrupt
  setup_ccp1(CCP_PWM | CCP_SHUTDOWN_AC_L | CCP_SHUTDOWN_BD_L);
  set_pwm1_duty((int16)0);

  setup_wdt(WDT_2304MS);
  SET_TRIS_A(0x18);
  // output_low(RELAY);
  output_low(PIN_A0);
  output_low(PIN_A1);
  output_low(PIN_A5);

  while (TRUE)
  {
    delay_ms(10);
    if (input(LED))
    {
      if (timer < 65535)
        timer++;
    }
    else
    {
      if (timer != 0)
      {
        if (timer < 250)
          relay_state = 0;
        else
          relay_state = 1;
        timer = 0;
      }
    }

    if (relay_state)
    {
      offtimer = 0;
      if (rtimer < 100)  {
        set_pwm1_duty((int16)1023);
        rtimer++;
      } else set_pwm1_duty((int16)2);
    }  else  {
      if (offtimer < 500) offtimer ++;
      else set_pwm1_duty((int16)0);
      rtimer = 0;
    }
  }
}
