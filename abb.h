#include <12F615.h>

#FUSES WDT                      //Watch Dog Timer
#FUSES PUT                      //Power Up Timer
#FUSES BROWNOUT_NOSL            //Brownout enabled during operation, disabled during SLEEP

#use delay(internal=4000000,restart_wdt)
#define LED   PIN_A4
#define RELAY   PIN_A2


